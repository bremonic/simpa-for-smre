var annotated_dup =
[
    [ "ArffReader", "classArffReader.html", "classArffReader" ],
    [ "ClassificationResult", "classClassificationResult.html", "classClassificationResult" ],
    [ "Condition", "classCondition.html", "classCondition" ],
    [ "DecisionTree", "classDecisionTree.html", "classDecisionTree" ],
    [ "Entropy", "classEntropy.html", null ],
    [ "ID3", "classID3.html", "classID3" ],
    [ "J48", "classJ48.html", null ],
    [ "Nominal", "classNominal.html", "classNominal" ],
    [ "Prediction", "classPrediction.html", "classPrediction" ],
    [ "Tag", "classTag.html", "classTag" ],
    [ "Type", "classType.html", "classType" ]
];