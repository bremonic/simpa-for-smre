var classArffReader =
[
    [ "ArffReader", "classArffReader.html#a1f8b6bffd6f2638d84bb785fcc314249", null ],
    [ "ArffReadData", "classArffReader.html#a23273ceec44d28a01f04e2c81a0a4159", null ],
    [ "ArffReadHeader", "classArffReader.html#ac44328cf09f7255cb4b23d5d2a9de519", null ],
    [ "ArffReadRelation", "classArffReader.html#a150fd9f71a8f29e4ab1ef9ab56047804", null ],
    [ "GenerateMapAttributes", "classArffReader.html#a5982cf892fb9f0f34a3306ec353646e6", null ],
    [ "getArrayAttributes", "classArffReader.html#ad9fb5de76444fb01c5cc1bf5008ea09e", null ],
    [ "getColumnsAttributes", "classArffReader.html#a3b6ac2e557b5f3826fce05b089acbc09", null ],
    [ "getHashTrain", "classArffReader.html#a77839fa4a8f320c34d6884f5a4f555f3", null ],
    [ "getMapAttributes", "classArffReader.html#ab63efb6bfbe03a914de61b7c2e96ad91", null ],
    [ "getRelation", "classArffReader.html#afbb402a146bf75afad60669b8b6b93c2", null ],
    [ "getTypeAttributes", "classArffReader.html#ae14ef985f90dac67ea40acc1d65d5695", null ],
    [ "arff_file", "classArffReader.html#a67398dd0718523f2150e1a7becb84d67", null ],
    [ "array_attributes", "classArffReader.html#a01f4084cc5167e5f1dd0dc124a46003e", null ],
    [ "columns_attributes", "classArffReader.html#a8ef8bf60a1d130784963abb01826f95d", null ],
    [ "hash_train", "classArffReader.html#a734d50a4d685667a15c21a4281bd5e4c", null ],
    [ "m_reader", "classArffReader.html#a21463aba71e421883b9998b9595550e2", null ],
    [ "map_attributes", "classArffReader.html#aea5d968d1cf1d564372f26fbce33757b", null ],
    [ "relation", "classArffReader.html#a9fa2515054ff9ba7c281e2e5e7f38e88", null ],
    [ "type_attributes", "classArffReader.html#a4c1e8e99d375b2b47fa62ad449fd1c05", null ]
];