var classDecisionTree =
[
    [ "DecisionTree", "classDecisionTree.html#a309a663d430bb3a501c066c5c659c3d4", null ],
    [ "DecisionTree", "classDecisionTree.html#ac117123285b3396703f33d5c67facd25", null ],
    [ "calculHeight", "classDecisionTree.html#a174c780e84d67d67528a320c1f7e0cb5", null ],
    [ "DecorTree", "classDecisionTree.html#a3b0a96e538aac212b408bc644f44b3c5", null ],
    [ "getChildren", "classDecisionTree.html#a83f708597b62f8d32b7ca0c3154c0373", null ],
    [ "getCondition", "classDecisionTree.html#a61dfa6695fdae78c11cddeaa31f1d229", null ],
    [ "getHeight", "classDecisionTree.html#a046e5b04583a1a44f9eeba465fde9470", null ],
    [ "getTag", "classDecisionTree.html#a0b0d9176b8ce5eb82a002ba58cbac15e", null ],
    [ "isLeaf", "classDecisionTree.html#a09db1ec40efd2d4eb6aa88afb2e77a04", null ],
    [ "removeChildren", "classDecisionTree.html#a0b8b239af579cc427541abba00277fdb", null ],
    [ "setHeight", "classDecisionTree.html#a536d33bad5a387c4e9817248c6562862", null ],
    [ "setTag", "classDecisionTree.html#ac2bd70eaa0dfdb4dd8da49fd76b0e23f", null ],
    [ "children", "classDecisionTree.html#a01f964275e34c838fdd7c0e23c88d0bf", null ],
    [ "condition", "classDecisionTree.html#a07353cc9185940b1ac8836c393b56e60", null ],
    [ "parent", "classDecisionTree.html#a945250402bed0bf9f76e76cb0c3a6d88", null ],
    [ "tag", "classDecisionTree.html#a6c39efdee8e0e532d7b3aade3d7b4b7b", null ]
];