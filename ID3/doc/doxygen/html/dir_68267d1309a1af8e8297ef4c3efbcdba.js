var dir_68267d1309a1af8e8297ef4c3efbcdba =
[
    [ "ArffReader.java", "ArffReader_8java.html", [
      [ "ArffReader", "classArffReader.html", "classArffReader" ]
    ] ],
    [ "ClassificationResult.java", "ClassificationResult_8java.html", [
      [ "ClassificationResult", "classClassificationResult.html", "classClassificationResult" ]
    ] ],
    [ "Condition.java", "Condition_8java.html", [
      [ "Condition", "classCondition.html", "classCondition" ]
    ] ],
    [ "DecisionTree.java", "DecisionTree_8java.html", [
      [ "DecisionTree", "classDecisionTree.html", "classDecisionTree" ]
    ] ],
    [ "Entropy.java", "Entropy_8java.html", [
      [ "Entropy", "classEntropy.html", null ]
    ] ],
    [ "ID3.java", "ID3_8java.html", [
      [ "ID3", "classID3.html", "classID3" ]
    ] ],
    [ "J48.java", "J48_8java.html", [
      [ "J48", "classJ48.html", null ]
    ] ],
    [ "Nominal.java", "Nominal_8java.html", [
      [ "Nominal", "classNominal.html", "classNominal" ]
    ] ],
    [ "Prediction.java", "Prediction_8java.html", [
      [ "Prediction", "classPrediction.html", "classPrediction" ]
    ] ],
    [ "Tag.java", "Tag_8java.html", [
      [ "Tag", "classTag.html", "classTag" ]
    ] ],
    [ "Type.java", "Type_8java.html", [
      [ "Type", "classType.html", "classType" ]
    ] ]
];