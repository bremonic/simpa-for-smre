var hierarchy =
[
    [ "ArffReader", "classArffReader.html", null ],
    [ "DecisionTree", "classDecisionTree.html", null ],
    [ "Entropy", "classEntropy.html", null ],
    [ "ID3", "classID3.html", null ],
    [ "J48", "classJ48.html", null ],
    [ "Nominal< T >", "classNominal.html", null ],
    [ "Tag", "classTag.html", null ],
    [ "Type", "classType.html", null ],
    [ "ArrayList", null, [
      [ "Prediction", "classPrediction.html", null ]
    ] ],
    [ "HashMap", null, [
      [ "ClassificationResult", "classClassificationResult.html", null ]
    ] ],
    [ "LinkedList", null, [
      [ "Condition", "classCondition.html", null ]
    ] ]
];