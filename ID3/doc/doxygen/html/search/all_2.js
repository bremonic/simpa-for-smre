var searchData=
[
  ['calculheight',['calculHeight',['../classDecisionTree.html#a174c780e84d67d67528a320c1f7e0cb5',1,'DecisionTree']]],
  ['children',['children',['../classDecisionTree.html#a01f964275e34c838fdd7c0e23c88d0bf',1,'DecisionTree']]],
  ['classificationresult',['ClassificationResult',['../classClassificationResult.html',1,'ClassificationResult'],['../classClassificationResult.html#a89df07962270a1d988d72acd22361448',1,'ClassificationResult.ClassificationResult()']]],
  ['classificationresult_2ejava',['ClassificationResult.java',['../ClassificationResult_8java.html',1,'']]],
  ['columns_5fattributes',['columns_attributes',['../classArffReader.html#a8ef8bf60a1d130784963abb01826f95d',1,'ArffReader']]],
  ['condition',['Condition',['../classCondition.html',1,'Condition'],['../classCondition.html#ab0a562f27900f1f0f9c4ae36d6d91c02',1,'Condition.Condition()'],['../classCondition.html#abffc5a794f79d0dd9e17a7b255a9a710',1,'Condition.Condition(Tag t)'],['../classCondition.html#a95084464558090b16be1936b465ab01b',1,'Condition.Condition(Condition c)'],['../classDecisionTree.html#a07353cc9185940b1ac8836c393b56e60',1,'DecisionTree.condition()']]],
  ['condition_2ejava',['Condition.java',['../Condition_8java.html',1,'']]]
];
