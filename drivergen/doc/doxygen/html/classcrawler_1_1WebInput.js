var classcrawler_1_1WebInput =
[
    [ "Type", "enumcrawler_1_1WebInput_1_1Type.html", "enumcrawler_1_1WebInput_1_1Type" ],
    [ "WebInput", "classcrawler_1_1WebInput.html#a914b7d87ef14b9496aa42db5ca66b423", null ],
    [ "WebInput", "classcrawler_1_1WebInput.html#aa67aa1dc6f4d3a2b3bc93a83a22603c1", null ],
    [ "WebInput", "classcrawler_1_1WebInput.html#aa7c31784e668a0e81e7f409c21a7504a", null ],
    [ "cleanRuntimeParameters", "classcrawler_1_1WebInput.html#aaf9cb648faafb93ed418247dc495e77b", null ],
    [ "equals", "classcrawler_1_1WebInput.html#af4eb0f0d8968520ef99f8d0607769789", null ],
    [ "getAddress", "classcrawler_1_1WebInput.html#af83169ffc93aa8c490bd2b7cffb38510", null ],
    [ "getMethod", "classcrawler_1_1WebInput.html#a21088bea0e755edc515ed6bff3f9c758", null ],
    [ "getNbValues", "classcrawler_1_1WebInput.html#a5e09a9cbec90a7d93b967e00378b5d22", null ],
    [ "getParams", "classcrawler_1_1WebInput.html#a8005c974c24e837abffd4b043d2ddbca", null ],
    [ "getType", "classcrawler_1_1WebInput.html#a9f840ed5e3061253c85cec8ac7e93bad", null ],
    [ "isAlmostEquals", "classcrawler_1_1WebInput.html#abe3d2a4eb5d1e538051c76ad909d43ba", null ],
    [ "setAddress", "classcrawler_1_1WebInput.html#a8837b95918e2fc4faafbcdee664a33ac", null ],
    [ "setMethod", "classcrawler_1_1WebInput.html#ae8f5ded026147ad0825c6e1faefdcf96", null ],
    [ "setNbValues", "classcrawler_1_1WebInput.html#a7175046eacd7196199b8e0c7805bb4b6", null ],
    [ "setParams", "classcrawler_1_1WebInput.html#a618fd4c920e068356c2a9ec926fa7bc9", null ],
    [ "setType", "classcrawler_1_1WebInput.html#ac7657ff8297c85d9b4e1d3b733ae550b", null ],
    [ "toString", "classcrawler_1_1WebInput.html#a3b990c2ade7639ee76c5474bf4e14b72", null ],
    [ "address", "classcrawler_1_1WebInput.html#ac7c12ca8b965c330f5adec1852ff6a36", null ],
    [ "method", "classcrawler_1_1WebInput.html#a1a47feef957ca94f6a1452021df88779", null ],
    [ "nbValues", "classcrawler_1_1WebInput.html#a922e5732f5bc94286b5cb71929d80eb3", null ],
    [ "params", "classcrawler_1_1WebInput.html#afbc5d932e0edde80e6405e5004818eeb", null ],
    [ "type", "classcrawler_1_1WebInput.html#a891a4376624c8dab9dc2f78a942596fa", null ]
];