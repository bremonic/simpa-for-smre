var classcrawler_1_1WebTransition =
[
    [ "WebTransition", "classcrawler_1_1WebTransition.html#a7ea73d825415b98e1ce7302853759c6c", null ],
    [ "getBy", "classcrawler_1_1WebTransition.html#afc2f8ab8d199e9e0814e3c4a169a0845", null ],
    [ "getFrom", "classcrawler_1_1WebTransition.html#ad77276ab355b29a2b9484e456d950946", null ],
    [ "getTo", "classcrawler_1_1WebTransition.html#affe51fd9ff5a244d8d2e21abe28b4d36", null ],
    [ "setBy", "classcrawler_1_1WebTransition.html#af91f66acdbbacca025c6d0aef51fe749", null ],
    [ "toString", "classcrawler_1_1WebTransition.html#a2f43b4744270d2dcbfe324e5503ff2df", null ],
    [ "by", "classcrawler_1_1WebTransition.html#a7353d08d973b6db9069a122b2e414c1f", null ],
    [ "from", "classcrawler_1_1WebTransition.html#a9147c41ca77c5f8ad8c45bcaf377f4fc", null ],
    [ "to", "classcrawler_1_1WebTransition.html#a26c258d04e4c95b19950cd0c93d727e4", null ]
];