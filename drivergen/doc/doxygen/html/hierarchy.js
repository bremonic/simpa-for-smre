var hierarchy =
[
    [ "tools.Base64", "classtools_1_1Base64.html", null ],
    [ "crawler.Configuration", "classcrawler_1_1Configuration.html", null ],
    [ "crawler.DriverGenerator", "classcrawler_1_1DriverGenerator.html", null ],
    [ "crawler.page.GenericTreeNode< T >", "classcrawler_1_1page_1_1GenericTreeNode.html", null ],
    [ "crawler.page.GenericTreeNode< String >", "classcrawler_1_1page_1_1GenericTreeNode.html", [
      [ "crawler.page.PageTreeNode", "classcrawler_1_1page_1_1PageTreeNode.html", null ]
    ] ],
    [ "tools.GraphViz", "classtools_1_1GraphViz.html", null ],
    [ "tools.HTTPData", "classtools_1_1HTTPData.html", null ],
    [ "tools.loggers.ILogger", "interfacetools_1_1loggers_1_1ILogger.html", null ],
    [ "tools.loggers.LogManager", "classtools_1_1loggers_1_1LogManager.html", null ],
    [ "main.drivergen.Main", "classmain_1_1drivergen_1_1Main.html", null ],
    [ "main.drivergen.Options", "classmain_1_1drivergen_1_1Options.html", null ],
    [ "crawler.WebInput.Type", "enumcrawler_1_1WebInput_1_1Type.html", null ],
    [ "tools.Utils", "classtools_1_1Utils.html", null ],
    [ "crawler.WebInput", "classcrawler_1_1WebInput.html", null ],
    [ "crawler.WebOutput", "classcrawler_1_1WebOutput.html", null ],
    [ "crawler.WebTransition", "classcrawler_1_1WebTransition.html", null ]
];