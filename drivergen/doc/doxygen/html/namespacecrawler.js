var namespacecrawler =
[
    [ "page", "namespacecrawler_1_1page.html", "namespacecrawler_1_1page" ],
    [ "Configuration", "classcrawler_1_1Configuration.html", "classcrawler_1_1Configuration" ],
    [ "DriverGenerator", "classcrawler_1_1DriverGenerator.html", "classcrawler_1_1DriverGenerator" ],
    [ "WebInput", "classcrawler_1_1WebInput.html", "classcrawler_1_1WebInput" ],
    [ "WebOutput", "classcrawler_1_1WebOutput.html", "classcrawler_1_1WebOutput" ],
    [ "WebTransition", "classcrawler_1_1WebTransition.html", "classcrawler_1_1WebTransition" ]
];