var searchData=
[
  ['webinput',['WebInput',['../classcrawler_1_1WebInput.html#a914b7d87ef14b9496aa42db5ca66b423',1,'crawler.WebInput.WebInput()'],['../classcrawler_1_1WebInput.html#aa67aa1dc6f4d3a2b3bc93a83a22603c1',1,'crawler.WebInput.WebInput(String link)'],['../classcrawler_1_1WebInput.html#aa7c31784e668a0e81e7f409c21a7504a',1,'crawler.WebInput.WebInput(HttpMethod m, String address, TreeMap&lt; String, List&lt; String &gt;&gt; params)']]],
  ['weboutput',['WebOutput',['../classcrawler_1_1WebOutput.html#a1f85b6db5e7a1b8a6b131e3d7b3200f4',1,'crawler.WebOutput.WebOutput()'],['../classcrawler_1_1WebOutput.html#a674402b9bbb15a737268dc9f81a44ee1',1,'crawler.WebOutput.WebOutput(Document doc, WebInput from, String limitSelector)'],['../classcrawler_1_1WebOutput.html#af11f2c99a2fd46b2750120f2e80bfc23',1,'crawler.WebOutput.WebOutput(String source, boolean raw, String limitSelector)']]],
  ['webtransition',['WebTransition',['../classcrawler_1_1WebTransition.html#a7ea73d825415b98e1ce7302853759c6c',1,'crawler::WebTransition']]]
];
