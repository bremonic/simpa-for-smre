package drivers.mealy;

import examples.mealy.Test3Mealy;

public class Test3Driver extends MealyDriver {

	public Test3Driver() {
		super(Test3Mealy.getAutomata());
	}
}
