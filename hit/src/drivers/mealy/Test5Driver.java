package drivers.mealy;

 import examples.mealy.Test5Mealy;

public class Test5Driver extends MealyDriver {

	public Test5Driver() {
		super(Test5Mealy.getAutomata());
	}
}
