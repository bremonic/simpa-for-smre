package drivers.mealy;

 import examples.mealy.Test6Mealy;

public class Test6Driver extends MealyDriver {

	public Test6Driver() {
		super(Test6Mealy.getAutomata());
	}
}
